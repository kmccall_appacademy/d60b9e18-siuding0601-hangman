require 'byebug'
class Hangman
  attr_reader :guesser, :referee, :board, :count

  def initialize(options = {})
    @guesser = options[:guesser]
    @referee = options[:referee]
    @board = nil
    @count = 0
  end

  def setup
    length = @referee.pick_secret_word
    @board = Array.new(length, nil)
    @guesser.register_secret_length(length)
  end

  def take_turn
    guess = @guesser.guess(@board)
    location = @referee.check_guess(guess)
    if location == []
      @count += 1
    else
      @count -= 1
    end
    self.update_board(location, guess)
    @guesser.handle_response(guess, location)
  end

  def update_board(location, guess)
    location.each do |i|
      @board[i] = guess
    end
    p @board
  end

  def play
    self.setup
    until @board.join == referee.secret_word || count == 10
      self.take_turn
    end
    if board.include?(nil)
      p "Sorry, the man is hung :("
    else
      p "You saved his life!"
    end
  end

end

class HumanPlayer
  attr_reader :dictionary,:secret_word, :guess, :already_guessed
  #Phase I
    def initialize(dictionary)
      @dictionary = dictionary
      @guess = ""
      @secret_word = ""
      @already_guessed = []
    end

    def pick_secret_word
      p "Pick a word length"
      word_length = gets.chomp.to_i
      if @dictionary.class == String
        lines = File.readlines(@dictionary).map(&:chomp)
        lines_length = lines.select {|w| w.length == word_length}
        @secret_word = lines_length.sample
      elsif @dictionary.class == Array
        @secret_word = @dictionary.select {|w| w.length == word_length}.sample
      end
      @secret_word.length
    end

    def register_secret_length(length)
      p "It is #{length} letters long"
    end

    def guess(_board)
      p "Make a guess!"
      gets.chomp.downcase
    end

    def check_guess(letter)
      if secret_word.include?(letter)
        p "You got it"
        return secret_word.split("").map.with_index {|al, i| i if al == letter}.compact
      else
        p "Wrong guess!"
        return []
      end
    end

    def handle_response(guess, _location)
      @already_guessed << guess.downcase
      p "What you have guessed:#{@already_guessed}"
    end


end

class ComputerPlayer

attr_reader :dictionary,:secret_word, :guess, :already_guessed, :candidate_words
#Phase I
  def initialize(dictionary)
    @dictionary = dictionary
    @guess = ""
    @secret_word = ""
    @already_guessed = []
      if @dictionary.class == String
        @candidate_words = File.readlines(@dictionary).map(&:chomp)
      elsif @dictionary.class == Array
        @candidate_words = dictionary
      end
  end

  def pick_secret_word
    if @dictionary.class == String
      lines = File.readlines(@dictionary)
      @secret_word = lines.sample.chomp
    elsif @dictionary.class == Array
      @secret_word = dictionary.sample
    end
    @secret_word.length
  end

  def register_secret_length(length)
    @candidate_words.reject! { |w| w.length != length}
  end

  def guess(board)
    freq = @candidate_words.join.split("").inject(Hash.new(0)) { |h,v| h[v] += 1; h }
    freq.delete_if {|k, v| board.include?(k)}
    freq.sort_by {|k, v| v}[-1][0]
  end

  def check_guess(letter)
    if secret_word.include?(letter)
      p "You got it"
      return secret_word.split("").map.with_index {|al, i| i if al == letter}.compact
    else
      p "Wrong guess!"
      return []
    end
  end

  def handle_response(guess, position = [])
    # debugger
    if position != []
      @candidate_words.each do |word|
        position.each do |i|
          if word.count(guess) != position.count
            @candidate_words.delete(word)
          elsif word.split("")[i] != guess
            @candidate_words.delete(word)
          end
        end
      end
    elsif position == [ ]
      @candidate_words.reject! {|word| word.include?(guess)}
    end
  end





end

#To play the game
me = HumanPlayer.new('lib/dictionary.txt')
pc = ComputerPlayer.new('lib/dictionary.txt')
options = {guesser:me, referee:pc}
game = Hangman.new(options)
game.play
